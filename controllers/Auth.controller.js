const createHttpError = require('http-errors')
const bcrypt = require("bcryptjs");
const crypto = require('crypto')

const Auth = require('../models/Auth.model')
const { Auth_Schema_Register, Auth_Schema_Login } = require('../helpers/Auth.Validate')
const { signAccessToken, signRefreshToken, verifyRefreshToken } = require('../helpers/jwt_helper')


exports.Auth_Register_Controllers = async(req, res, next) => {

    try {

        //   let { name, email, password } = req.body
        //   if (!name || !email || !password) throw createHttpError.BadRequest('ກະລຸນາໃສ່ຂໍ້ມູນໃຫ້ຄົບ'); //BadRequest status 400
        //todo for validate data before save data 
        //from @helpers/auth
        const result = await Auth_Schema_Register.validateAsync(req.body)
            //check name and email 
        const checkname = await Auth.findOne({ where: { name: result.name } })
        if (checkname) throw createHttpError.Conflict(`${result.name} is already been register `)

        const checkemail = await Auth.findOne({ where: { email: result.email } })
        if (checkemail) throw createHttpError.Conflict(`${result.email} is already been register `)

        //todo password bcrypt
        var salt = bcrypt.genSaltSync(10);
        result.password = bcrypt.hashSync(result.password, salt)

        //todo gen uuid 
        //1 person can use uuid id one id
        const uuid = crypto.randomBytes(4).toString("hex")
        const cryp_to = result.name + "_uuid_" + uuid
        const signin = {
            name: result.name,
            email: result.email,
            password: result.password,
            uuid: cryp_to
        }
        const register = await Auth.create(signin);
        // const accessToken = await signAccessToken(register.id)

        // const refreshToken = await signRefreshToken(register.id)
        res.json({
            status: true,
            message: "Register successfully"
        })



    } catch (error) {
        //!ສາເຫດທີເຮົາໃສ່ໃຫ້ error 422 ນັ້ນເພາະບໍ່ໃຫ້ມັນມີບັນຫາກັບ server
        if (error.isJoi === true) error.status = 422
        next(error)
    }

}

exports.Auth_Login_Controller = async(req, res, next) => {
    try {
        //check validate date
        //from  folder helpers
        const result = await Auth_Schema_Login.validateAsync(req.body);
        //check user on database
        const user = await Auth.findOne({ where: { email: result.email } });
        if (!user) throw createHttpError.NotFound("User not registered")
            //compare password 
        let Math = bcrypt.compareSync(result.password, user.password);
        if (!Math) throw createHttpError(401, "Invalid email/password")

        //asccess token 
        const token = await signAccessToken(user)
            // console.log(req.bod);

        //refresh token 
        const refreshToken = await signRefreshToken(user.id)
        res.send({
            status: true,
            message: "Login Successfully",
            access_token: token
        })
    } catch (error) {
        // ຖ້າ email or password ຈະໃຫ້ມັນເເຈ້ງເຕືອນວ່າບໍ່ຖືກຕ້ອງ ຕາມເງືອນໄຂທີ່ໃດ້ validate 
        if (error.isJoi === true) return next(createHttpError.BadRequest("Invalid email/password"))
        next(error)
    }
}

//refreshToken 
exports.Auth_Refresh_Token = async(req, res, next) => {
    try {
        // console.log("resh", req.body);
        const refreshToken = req.body
            // console.log(refreshToken);
        if (!refreshToken) throw createHttpError.BadRequest();

        //verify refresh token 
        const userId = await verifyRefreshToken(refreshToken)
        const accessToken = await signAccessToken(userId)
        const refToken = await signRefreshToken(userId)
        res.send({ token: accessToken, refreshToken: refToken })
    } catch (error) {
        next(error)
    }
}

exports.Auth_Profile = async(req, res, next) => {
    try {

        //   let userId = req.playload.aud
        //   const getprofile = await Auth.findByPk(userId);
        //   res.send({ getprofile })
        const { refreshToken } = req.body
        if (!refreshToken) throw createHttpError.BadRequest()
        const userId = await verifyRefreshToken(refreshToken)
        console.log(refreshToken)
    } catch (error) {
        next(error)
    }
}

exports.Auth_Logout = async(req, res, next) => {

}