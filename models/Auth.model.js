const { Sequelize, DataTypes } = require("sequelize");
const db = require("../config/database");

module.exports = db.define("auths_users", {
    id: {
        field: "userId",
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        field: "name",
        type: DataTypes.STRING,
    },
    email: {
        field: "email",
        type: DataTypes.STRING,
    },
    password: {
        field: "password",
        type: DataTypes.STRING,
    },
    uuid: {
        field: "uuid",
        type: DataTypes.STRING,
    },
});