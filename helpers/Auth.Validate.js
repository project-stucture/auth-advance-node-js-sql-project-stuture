const Joi = require('joi');


const Auth_Schema_Register = Joi.object({
    name: Joi.string().min(3).max(30).required(),
    email: Joi.string().email().lowercase().required(),
    password: Joi.string().min(6)
})
const Auth_Schema_Login = Joi.object({
    // name: Joi.string().min(3).max(30).required(),
    email: Joi.string().email().lowercase().required(),
    password: Joi.string().min(6)
})

module.exports = {
    Auth_Schema_Register,
    Auth_Schema_Login
}