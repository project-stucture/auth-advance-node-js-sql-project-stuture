const jwt = require('jsonwebtoken');
const createHttpError = require('http-errors');
const redis = require("redis");
const client = redis.createClient();

client.on("error", function(error) {
    console.error(error);
});

module.exports = {
    signAccessToken: (user) => {

        return new Promise((resolve, reject) => {
            console.log(user);
            const playload = {
                // aud: user.userId, //from id ?
                password: user.password,
                uuid: user.uuid,
                email: user.email
            }
            console.log(playload);
            const secret = process.env.ACCESS_TOKEN_SECRET
            const option = {
                expiresIn: '1m', //time token 1h ຫມົດອາຍຸ
            }
            jwt.sign(playload, secret, option, (err, token) => {
                if (err) {
                    reject(createHttpError.InternalServerError())
                }
                client.set(token, token, 'EX', 30, (err, reply) => {
                    console.log(err);
                    console.log(reply);
                });
                resolve(token)
            });


        })
    },


    VerifyAccessToken: (req, res, next) => {

        //@ເຮົາຕ້ອງກວດສອບ header Authorization
        if (!req.headers['authorization']) return next(createHttpError.Unauthorized());

        //@ເຮົາຕ້ອງການເວລາເຮົາເອົາກວດສອບ ຫລື ເອົາໄປນຳໃຊ້ນັ້ນ ເຮົາ
        //ບໍ່ຕ້ອງການໃຫ້ Bearer ນັ້ນມັນຕິດກັນກັບ token ຕ້ອງຍະວ່າງບ່ອນຫນື່ງ
        const authHeader = req.headers['authorization']
        const BearerToken = authHeader.split(' ')
        const token = BearerToken[1]

        //@ verify token
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, playload) => {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    return next(createHttpError.Unauthorized('ການເຂົ້າສູ່ລະບົບນັ້ນໄດ້ຫມົດອາຍຸເເລ້ວ.... ກະລຸນາເຂົ້າສູ່ລະບົບໄຫມ່'))
                } else {
                    return next(createHttpError.Unauthorized(err.message))
                }
            }
            req.playload = playload
            next();
        });
    },
    // refresh token

    RefreshToken: async(req, res, next) => {
        const authHeader = req.headers['authorization']
        const BearerToken = authHeader.split(' ')
        const token = BearerToken[1]
        console.log("token reshesh", token)

        const userid = module.exports.verifyToken(token);
        client.get(token, async(err, reply) => {
            if (err) {
                console.log(err);
            }
            if (reply) {
                const newtoken = await module.exports.signAccessToken(userid);
                res.setHeader('refreshtoken', newtoken);
                client.del(token, (err, reply) => {
                    console.log(err);
                    console.log(reply);
                });
                client.set(newtoken, newtoken, 'EX', 30, (err, reply) => {
                    console.log(err);
                    console.log(reply);
                })
                next();
            } else {
                next(createHttpError.Unauthorized("err something"))
            }

        });

    },

    // Refresh Tokens คือแนวคิดของการแก้ไขปัญหาเมื่อ Access Token หมดอายุเร็ว ... 
    // ข้อมูลใน Access Token จะได้รับการ Update อยู่เสมอ สามารถยกเลิกการเข้าใช้งานของ User (Revoke access tokens)
    //  ไม่จำเป็นต้องให้ User ทำการ Authen บ่อยๆ

    signRefreshToken: (userId) => {
        return new Promise((resolve, reject) => {
            const playload = {
                aud: userId, //from id ?
            }
            const secret = process.env.REFRESH_TOKEN_SECRET
            const option = {
                expiresIn: '1y', //time token 1h ຫມົດອາຍຸ
            }
            jwt.sign(playload, secret, option, (err, token) => {
                if (err) {
                    reject(createHttpError.InternalServerError())
                }

                resolve(token)
            })
        })
    },

    //verifyRefreshToken 
    verifyRefreshToken: (refreshToken) => {
        return new Promise((resolve, reject) => {
            console.log(refreshToken);
            jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, playload) => {
                if (err) return reject(createHttpError.Unauthorized())
                const userId = playload;
                resolve(userId)
            })
        })

    },
    verifyToken: (refreshToken) => {
        return new Promise((resolve, reject) => {
            jwt.verify(refreshToken, process.env.ACCESS_TOKEN_SECRET, (err, playload) => {
                if (err) return reject(createHttpError.Unauthorized())
                const userId = playload.aud;
                resolve(userId)
            })
        })
    }
}