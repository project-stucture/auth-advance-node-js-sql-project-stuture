const redis = require("redis");
const client = redis.createClient();

client.on("error", function(error) {
    console.error(error);
});

client.set("1111", JSON.stringify({ userid: 1, username: 'abc' }), (err, reply) => {
    console.log(err);
    console.log(reply);
});
client.get("1111", (err, reply) => {
    console.log(err);
    console.log(JSON.parse(reply));
});