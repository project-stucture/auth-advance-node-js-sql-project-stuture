 const express = require('express')
 const morgan = require("morgan")
 const creatError = require('http-errors')
 require('dotenv').config()

 const app = express();
 app.use(morgan("dev"))

 const cors = require("cors");
 const bodypaser = require("body-parser");
 app.use(express.json());
 app.use(cors());
 app.use(express.urlencoded({ extended: true }));
 app.use(bodypaser.json());
 //!router import 
 const AuthRoute = require('./router/Auth.route')
 app.use('/auth', AuthRoute)

 const { VerifyAccessToken, RefreshToken } = require('./helpers/jwt_helper')

 //local server
 app.get('/', VerifyAccessToken, async(req, res) => {
     // console.log(req.headers['authorization'])
     res.json({ message: "WELCOME TO AUTH API-ADVANCE" })
 })

 //local server
 app.get('/jwt-test', VerifyAccessToken, RefreshToken, async(req, res) => {
         // console.log(req.headers['authorization'])
         res.json({ message: "Test Jwt token", refreshtoken: res.getHeader('refreshtoken') });

     })
     //Error response 
 app.use(async(req, res, next) => {
         //!nomal use Error
         //   const error = new Error("Not found")
         //   error.status = 404
         //   next(error)

         //!use http-errors 
         next(creatError.NotFound())

     })
     //todo show error response
 app.use((err, req, res, next) => {
         res.status(err.status || 500)
         res.send({
             error: {
                 status: err.status || 500,
                 message: err.message,
             }
         })
     })
     //!connect database
 const db = require('./config/database')
 db.authenticate()
     .then(() => {
         console.log('database connected.....')
     })
     .catch(err => {
         console.log(err)
     })
     //prot server 
 const port = process.env.PORT || 9000;
 app.listen(port, () => {
     console.log(`server run on port ${port}`)
 })