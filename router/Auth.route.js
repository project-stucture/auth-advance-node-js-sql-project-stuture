const express = require('express')
const router = express.Router()
const { Auth_Register_Controllers, Auth_Login_Controller, Auth_Refresh_Token, Auth_Profile } = require('../controllers/Auth.controller')
const { VerifyAccessToken } = require('../helpers/jwt_helper')
router.route('/login').post(Auth_Login_Controller)
router.route('/register').post(Auth_Register_Controllers)

router.route('/refresh-token').post(Auth_Refresh_Token)
router.route('/profile').get(VerifyAccessToken, Auth_Profile)
router.delete('/logout', async(req, res, next) => {
    res.send("Logout")
})


module.exports = router